import 'package:flutter/material.dart';

import '../color/app_color.dart';

class AppTypography {


  static const _roboto = 'Signika';

  final TextStyle kHeading24 = const TextStyle(
      fontFamily: _roboto,
      fontSize: 25,
      fontWeight: FontWeight.w600,
      color: AppColors.text);

  final TextStyle kHeading26 = const TextStyle(
      fontFamily: _roboto,
      fontSize: 17,
      fontWeight: FontWeight.w600,
      color: AppColors.greyColor);

  final TextStyle kHeading22 = const TextStyle(
      fontFamily: _roboto,
      fontSize: 12,
      fontWeight: FontWeight.w600,
      color: AppColors.text);


  final TextStyle text = const TextStyle(
      fontFamily: _roboto,
      fontSize: 17,
      fontWeight: FontWeight.w400,
      color: AppColors.text);

  final TextStyle body = const TextStyle(
      fontFamily: _roboto,
      fontSize: 22,
      fontWeight: FontWeight.w400,
      color: AppColors.text);
  final TextStyle kHeading20 = const TextStyle(
      fontFamily: _roboto,
      fontSize: 25,
      fontWeight: FontWeight.w400,
      color: AppColors.profilText);

  final TextStyle hintext = const TextStyle(
      fontFamily: _roboto,
      fontSize: 16,
      fontWeight: FontWeight.w400,
      color: AppColors.hintText);

  final TextStyle title = const TextStyle(
      fontFamily: _roboto,
      fontSize: 12,
      fontWeight: FontWeight.w500,
      color: AppColors.text);
  final TextStyle subtitle = const TextStyle(
      fontFamily: _roboto,
      fontSize: 12,
      fontWeight: FontWeight.w300,
      color: AppColors.text);


}
