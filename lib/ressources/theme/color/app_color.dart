import 'package:flutter/material.dart';


class AppColors {
  const AppColors._();

  static const Color primaryColor = Color(0xFF91C788);
  static const Color secondary = Color(0xFFFF9385);
  static const Color inputBorder = Color(0xFFF4F4F4);
  static const Color hintText = Color(0xFF999999);
  static const Color background = Color(0xFFFFFFFF);
  static const Color text = Color(0xFF000000);
  static const Color black = Color(0xFF0D0D0D);
  static const Color textBlack = Color(0xFF330600);
  static const Color text1 = Color(0xFFFF8473);
  static const Color text2 = Color(0xFF707070);
  static const Color bottom = Color(0xFFCFE7CB);
  static const Color dotColor = Color(0xFFFFC0B8);
  static const Color greyColor = Color(0xFF7C7C7C);
  static const Color profilText = Color(0xFF272727);
  static const Color gray = Color(0xFFA1A1A1);
  static const Color cardColor = Color(0xFFFFF8EE);
  static const Color greyText = Color(0xFF7B7B7B);
  static const Color violet = Color(0xFFC6C4DE);
  static const Color lightViolet = Color(0xFF9E9BC7);
  static const Color pink = Color(0xFFFFF2F0);
  static const Color lightGray = Color(0xFF4D0A00);
  static const Color clear = Color(0xff4130f26);
  static const Color noFound = Color(0xff696969);
  static const Color yellow = Color(0xffFFD485);
  static const Color lightGreen = Color(0xffEFF7EE);


}