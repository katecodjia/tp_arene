import 'package:flutter/material.dart';

import '../../utils/keys.dart';
import '../storage/app_storage.dart';

class AppService extends ChangeNotifier {
  AppService._();

  static final AppService instance = AppService._();

  double _statusBarHeight = 0.0;

  bool _home = false;
  bool _initialized = false;

  bool get home => _home;

  bool get initialized => _initialized;

  double get statusBarHeight => _statusBarHeight;

  String initialRoute = "/login";

  set homeState(bool state) {
    AppStorage.instance.write(key: StorageKeys.home, value: state);
    _home = state;
    notifyListeners();
  }

  set initialized(bool value) {
    _initialized = value;
    notifyListeners();
  }

  set setStatusBarHeight(double value) {
    _statusBarHeight = value;
    notifyListeners();
  }

  Future<void> onAppStart() async {
    _home =
        await AppStorage.instance.read(key: StorageKeys.home) ?? false;
    _initialized = true;
    notifyListeners();
  }
}
