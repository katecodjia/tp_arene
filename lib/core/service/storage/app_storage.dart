import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AppStorage<T> {
  AppStorage._();

  static final AppStorage instance = AppStorage._();

  // Create storage
  final storage = const FlutterSecureStorage();

  T? validateResult(dynamic data) {
    if (data is T) return data;
    return null;
  }

  // Write value
  Future<void> write({
    required String key,
    required dynamic value,
  }) async {
    String encoded = jsonEncode({"data": value});
    return await storage.write(key: key, value: encoded);
  }

  // Read value
  Future<dynamic> read({required String key}) async {
    try {
      String? value = await storage.read(key: key);

      if (value != null) {
        Map decoded = jsonDecode(value);
        dynamic data = decoded['data'];
        return validateResult(data);
      }
      return null;
    } catch (e) {
      if (kDebugMode) {
        print("ERROR: read $e");
      }
      await storage.delete(key: key);
      return null;
    }
  }

  Future<void> deleteAll() async => await storage.deleteAll();

  Future<void> delete({required String key}) async => await storage.delete(key: key);
}
