import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'route_utils.dart';
import 'package:tp_arene/export.dart';

class AppRouter {
  static final _rootNavigatorKey = GlobalKey<NavigatorState>();
  static final _shellNavigatorKey = GlobalKey<NavigatorState>();

  /// The route configuration.
  static final GoRouter _goRouter = GoRouter(
    navigatorKey: _rootNavigatorKey,
    debugLogDiagnostics: true,
    initialLocation: AppPage.intro.toPath,
    errorBuilder: (context, state) {
      return const Scaffold(
        body: Center(
          child: Text(
            " Oups route error :)",
            style: TextStyle(color: Colors.black),
          ),
        ),
      );
    },
    routes: [
      ShellRoute(
          navigatorKey: _shellNavigatorKey,
          builder: (context, state, child) {
            return child;
          },
          routes: [
            GoRoute(
                path: AppPage.home.toPath,
                name: AppPage.home.toName,
                builder: (context, state) => const HomeScreen(),
                routes: [
                  GoRoute(
                    path: "scan",
                    pageBuilder: (context, state) =>
                     NoTransitionPage(child: ScanScreen()),
                    routes: [

                    ]),
                  GoRoute(
                    path: "favorite",
                    pageBuilder: (context, state) =>
                    const NoTransitionPage(child: FavoriteScreen()),
                    routes: [
                    ]
                  ),
                  GoRoute(
                      path: "search",
                      pageBuilder: (context, state) =>
                      const NoTransitionPage(child: SearchScreen()),
                      routes: [
                        GoRoute(
                          path: "searchPage",
                          name: "searchPage",
                          pageBuilder: (context, state) =>
                          const NoTransitionPage(child: SearchPage()),
                        ),
                      ]

                  ),
                  GoRoute(
                      path: "profil",
                      pageBuilder: (context, state) =>
                      const NoTransitionPage(child: ProfileScreen()),
                      routes: [
                      ]
                  ),

                ]),
          ]),


      GoRoute(
        path: AppPage.dashboard.toPath,
        name: AppPage.dashboard.toName,
        builder: (context, state) {
          return const BottomNavigation();
        },
      ),
      GoRoute(
        path: AppPage.intro.toPath,
        name: AppPage.intro.toName,
        builder: (context, state) {
          return const SplashScreen();
        },
      ),

      GoRoute(
        path: AppPage.introScreen.toPath,
        name: AppPage.introScreen.toName,
        builder: (context, state) {
          return const IntroScreen();
        },
      ),


    ],
  );

  static GoRouter get router => _goRouter;
}
