enum AppPage {
  intro,
  introScreen,
  dashboard,
  home,
  search,
  scan,
  favorite,
  profil,
  searchPage,
  research,

}

extension AppPageExtension on AppPage {
  String get toPath {
    switch (this) {
      case AppPage.intro:
        return "/intro";
      case AppPage.introScreen:
        return "/introScreen";
      case AppPage.dashboard:
        return "/dashboard";
      case AppPage.home:
        return "/home";
      case AppPage.search:
        return "/search";
      case AppPage.scan:
        return "/scan";
      case AppPage.favorite:
        return "/favorite";
      case AppPage.profil:
        return "/profil";
      case AppPage.searchPage:
        return "/searchPage";
        default:
        return "/";
    }
  }

  String get toName {
    switch (this) {
      case AppPage.intro:
        return "INTRO";
      case AppPage.introScreen:
        return "INTROSCREEN";
      case AppPage.dashboard:
        return "DASHBOARD";
      case AppPage.home:
        return "HOME";
      case AppPage.search:
        return "SEARCH";
      case AppPage.scan:
        return "SCAN";
      case AppPage.favorite:
        return "FAVORITE";
      case AppPage.profil:
        return "PROFIL";
      case AppPage.searchPage:
        return "SEARCHPAGE";
      default:
        return "HOME";
    }
  }
}