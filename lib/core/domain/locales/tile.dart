import 'package:tp_arene/export.dart';
class Tile{
  final String img;
 final  String text;
 final  String? protein;
 final  String? calorie;
 final  String? fat;
 final  String? carbs;
 final  String? details;
 final List<Ingredient>? ingr;



  Tile({this.protein, this.calorie, this.fat, this.carbs, this.details, this.ingr, required this.img, required this.text});
}

class Ingredient{
  final String image;

  Ingredient({required this.image});
}
List<Tile> myTile=[
  Tile(img: $appAssets.svgs.icProfil, text: "Edit Profile"),
  Tile(img: $appAssets.svgs.rate, text: "Renew Plans"),
  Tile(img: $appAssets.svgs.setting, text: "Settings"),
  Tile(img: $appAssets.svgs.terms, text: "Terms & Privacy Policy"),
  Tile(img: $appAssets.svgs.logout, text: "Log Out"),
];

List<Tile> myFavorite=[
  Tile(protein: "450g",calorie: "220g",fat: "100g",carbs: "49g",img: $appAssets.svgs.strawberry, text: "Fruits",details: "A hamburger (also burger for short) is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread",ingr: myIngredient),
  Tile(protein: "450g",calorie: "220g",fat: "100g",carbs: "49g",img: $appAssets.svgs.vegetables, text: "Vegetables",details: "A hamburger (also burger for short) is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread",ingr: myIngredient),
  Tile(protein: "450g",calorie: "220g",fat: "100g",carbs: "49g",img: $appAssets.svgs.cookie, text: "Fruits",details: "A hamburger (also burger for short) is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread",ingr: myIngredient),
  Tile(protein: "450g",calorie: "220g",fat: "100g",carbs: "49g",img: $appAssets.svgs.ice, text: "Vegetables",details: "A hamburger (also burger for short) is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread",ingr: myIngredient),
  Tile(protein: "450g",calorie: "220g",fat: "100g",carbs: "49g",img: $appAssets.svgs.burger, text: "Fruits",details: "A hamburger (also burger for short) is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread",ingr: myIngredient),
  Tile(protein: "450g",calorie: "220g",fat: "100g",carbs: "49g",img: $appAssets.svgs.pizza, text: "Vegetables",details: "A hamburger (also burger for short) is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread",ingr: myIngredient),
  Tile(protein: "450g",calorie: "220g",fat: "100g",carbs: "49g",img: $appAssets.svgs.hotDog, text: "Vegetables",details: "A hamburger (also burger for short) is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread",ingr: myIngredient),

];
List<Ingredient> myIngredient=[
  Ingredient(image: $appAssets.svgs.bread),
  Ingredient(image: $appAssets.svgs.tomato),
  Ingredient(image: $appAssets.svgs.salad),
];

List<String> favorite =[

];

