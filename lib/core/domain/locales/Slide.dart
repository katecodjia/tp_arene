import 'package:tp_arene/export.dart';
class Slide{
 final String img;
 final String title;
 final String subtitle;
  final String? kcal;
  Slide({this.kcal,required this.img, required this.title, required this.subtitle});
}

List<Slide> myList=[
  Slide(
      img: $appAssets.svgs.intro1,
      title: "Eat Healthy",
      subtitle: "Maintaining good health should be the primary focus of everyone."
  ),
  Slide(
      img: $appAssets.svgs.intro2,
      title: "Healthy Recipes",
      subtitle: "Browse thousands of healthy recipes from all over the world."
  ),
  Slide(
      img: $appAssets.svgs.intro3,
      title: "Track Your Health",
      subtitle: "With amazing inbuilt tools you can track your progress."
  ),

];

List<Slide> myHot=[
  Slide(img: $appAssets.imgs.pepper, title: "The Pumkins Secrets", subtitle: "The Pumkins Secrets"),
  Slide(img: $appAssets.imgs.greenSecret, title: "Green Secrets", subtitle: "The Pumkins Secrets"),
  Slide(img: $appAssets.imgs.pepper, title: "The Pumkins Secrets", subtitle: "The Pumkins Secrets"),
  Slide(img: $appAssets.imgs.greenSecret, title: "Green Secrets", subtitle: "The Pumkins Secrets"),
  Slide(img: $appAssets.imgs.pepper, title: "The Pumkins Secrets", subtitle: "The Pumkins Secrets"),
  Slide(img: $appAssets.imgs.greenSecret, title: "Green Secrets", subtitle: "The Pumkins Secrets"),
];

List<String> myTrending=[
  "best vegetable recipes",
  "cool season vegetables",
  "chicken recipes with eggs",
  "soups",
];

List<Slide> recipes =[
  Slide(img: $appAssets.svgs.chopped, kcal: "250 Kcal",title: "Chopped Spring Ramen", subtitle: "Scallions & tomatoes"),
  Slide(img: $appAssets.svgs.chicken, kcal: "450 Kcal",title: "Chicken Tandoori", subtitle: "Chicken & Salad"),
];