import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppUiOverlay {
  static SystemUiOverlayStyle get light => SystemUiOverlayStyle.light.copyWith(
    systemNavigationBarColor: Colors.white,
    systemNavigationBarIconBrightness: Brightness.dark,
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.dark,
    statusBarBrightness: Brightness.light,
    systemNavigationBarContrastEnforced: false,
  );

}