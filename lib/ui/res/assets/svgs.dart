class Svgs {
  //into
  final intro1 = "assets/svg/intro1.svg";
  final intro2 = "assets/svg/intro2.svg";
  final intro3 = "assets/svg/intro3.svg";
  //home
  final cheese = "assets/svg/home_cheese.svg";
  final strawberry = "assets/svg/strawberry.svg";
  final vegetables = "assets/svg/vegetables.svg";
  final bread = "assets/svg/bread.svg";
  final burger = "assets/svg/burger.svg";
  final salad = "assets/svg/salad.svg";
  final tomato = "assets/svg/tomato.svg";
  final searchNotFound = "assets/svg/food_not_found.svg";
  final home = "assets/svg/ic_home.svg";
  final homeActive = "assets/svg/ic_home_active.svg";
  final search = "assets/svg/ic_search.svg";
  final searchActive = "assets/svg/ic_search_active.svg";
  final favorite = "assets/svg/ic_heart.svg";
  final favoriteActive = "assets/svg/ic_heart_active.svg";
  final profil = "assets/svg/ic_profile.svg";
  final profilActive ="assets/svg/ic_profile_active.svg";
  final camera ="assets/svg/ic_camera.svg";
  final profilStatus="assets/svg/profil_status.svg";
  final logout="assets/svg/profil/logout.svg";
  final icProfil="assets/svg/profil/profile.svg";
  final rate ="assets/svg/profil/rate.svg";
  final setting = "assets/svg/profil/setting.svg";
  final terms = "assets/svg/profil/terms.svg";
  //favorie
  final cookie = "assets/svg/cookie.svg";
  final hotDog ="assets/svg/hot_dog.svg";
  final ice ="assets/svg/ice.svg";
  final pizza ="assets/svg/pizza.svg";
  final foodNotFound ="assets/svg/food_found.svg";
  final chopped ="assets/svg/chopped.svg";
  final chicken ="assets/svg/chicken.svg";
}