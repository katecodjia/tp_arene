import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';

import '../../../core/router/route_utils.dart';
import '../../common/button/app_inkwell.dart';
class BottomNavigation extends StatefulWidget {
  const BottomNavigation({Key? key}) : super(key: key);

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int selectTab =0;
  late final CameraDescription camera;
  @override
  Widget build(BuildContext context) {
    final homeDashBoardProvider = Provider.of<DashboardProvider>(context);

    return  Scaffold(
      body: Consumer<DashboardProvider>(
        builder: (context, provider, child) {
          if(provider.path==AppPage.home){
            return const HomeScreen();
          }else if(provider.path==AppPage.search){
            return const SearchScreen();
          }else if(provider.path==AppPage.scan){
            return  const ScanScreen();
          }else if(provider.path==AppPage.favorite){
            return const FavoriteScreen();
          }else if(provider.path==AppPage.profil){
            return const ProfileScreen();
          }else if(provider.path==AppPage.searchPage){
            return const SearchPage();
          }
          else{
            return const HomeScreen();
          }
        },
      ),

      bottomNavigationBar: Consumer<DashboardProvider>(
        builder: (context, provider, child) {
          return provider.path==AppPage.scan ?
              const SizedBox.shrink()
            :BottomAppBar(
            color: AppColors.background,
            child: Container(
              height: 90,
              decoration: BoxDecoration(
                  // boxShadow: [
                  //   BoxShadow(
                  //       offset: const Offset(0,-5),
                  //       color: AppColors.black.withOpacity(.05),
                  //       blurRadius: 48,
                  //       spreadRadius: 0
                  //   )
                  // ]
              ),
              child: Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.only(left: 30, right: 30,bottom: 30,top: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    AppInkwell(
                      onTap: () {
                        if(provider.path!=AppPage.home || homeDashBoardProvider.path!=AppPage.home){
                          provider.setIndex(0);
                          homeDashBoardProvider.setPath(AppPage.home);
                          provider.setPath(AppPage.home);
                        }
                      },
                      child: SvgPicture.asset(provider.index==0?$appAssets.svgs.homeActive:$appAssets.svgs.home),

                    ),
                    AppInkwell(
                      onTap: () {
                        if(provider.path!=AppPage.search){
                          provider.setIndex(2);
                          provider.setPath(AppPage.search);
                        }
                      },
                      child: SvgPicture.asset(provider.index==2?$appAssets.svgs.searchActive:$appAssets.svgs.search),

                    ),
                    AppInkwell(
                      onTap: () {
                        if(provider.path!=AppPage.scan){
                          provider.setIndex(3);
                          provider.setPath(AppPage.scan);
                        }
                      },
                      child: provider.index==3?
                      Container(
                                  padding: const EdgeInsets.all(10),
                                  height: 48,
                                  width: 48,
                                  decoration: const BoxDecoration(
                                    color: AppColors.primaryColor,
                                      shape: BoxShape.circle,

                                  ),
                                  child: SvgPicture.asset($appAssets.svgs.camera),
                                )
                          : Container(
                        padding: const EdgeInsets.all(10),
                        height: 48,
                        width: 48,
                        decoration: const BoxDecoration(
                          color: AppColors.primaryColor,
                          shape: BoxShape.circle,

                        ),
                        child: SvgPicture.asset($appAssets.svgs.camera),
                      ),
                    ),
                    AppInkwell(
                      onTap: () {
                        if(provider.path!=AppPage.favorite){
                          provider.setIndex(4);
                          provider.setPath(AppPage.favorite);
                        }
                      },
                      child:
                      SvgPicture.asset(provider.index==4?$appAssets.svgs.favoriteActive:$appAssets.svgs.favorite),

                    ),
                    AppInkwell(
                      onTap: () {
                        if(provider.path!=AppPage.profil){
                          provider.setIndex(5);
                          provider.setPath(AppPage.profil);
                        }
                      },
                      child: SvgPicture.asset(provider.index==5?$appAssets.svgs.profilActive:$appAssets.svgs.profil),

                    ),

                  ],
                ),
              ),
            ),
          );
        },
      ),

      // bottomNavigationBar: BottomNavigationBar(
      //   type: BottomNavigationBarType.fixed,
      //   currentIndex: selectTab,
      //   onTap: (int index){
      //             setState(() {
      //               selectTab=index;
      //             });
      //   },
      //   backgroundColor: AppColors.background,
      //   items: [
      //     BottomNavigationBarItem(
      //       label: "",
      //         icon: SvgPicture.asset($appAssets.svgs.home),
      //       activeIcon: SvgPicture.asset($appAssets.svgs.homeActive)
      //     ),
      //     BottomNavigationBarItem(
      //         label: "",
      //         icon: SvgPicture.asset($appAssets.svgs.search),
      //       activeIcon: SvgPicture.asset($appAssets.svgs.searchActive)
      //     ),
      //     BottomNavigationBarItem(
      //         label: "",
      //         icon: Container(
      //           padding: const EdgeInsets.all(10),
      //           height: 48,
      //           width: 48,
      //           decoration: const BoxDecoration(
      //             color: AppColors.primaryColor,
      //               shape: BoxShape.circle,
      //
      //           ),
      //           child: SvgPicture.asset($appAssets.svgs.camera),
      //         ),
      //       activeIcon: Container(
      //         padding: const EdgeInsets.all(10),
      //         height: 48,
      //         width: 48,
      //         decoration: const BoxDecoration(
      //             color: AppColors.primaryColor,
      //          // borderRadius: BorderRadius.circular(20),
      //                 shape: BoxShape.circle
      //         ),
      //         child: SvgPicture.asset($appAssets.svgs.camera),
      //       )
      //     ),
      //     BottomNavigationBarItem(
      //         label: "",
      //
      //         icon: SvgPicture.asset($appAssets.svgs.favorite),
      //       activeIcon: SvgPicture.asset($appAssets.svgs.favoriteActive)
      //     ),
      //     BottomNavigationBarItem(
      //         label: "",
      //
      //         icon: SvgPicture.asset($appAssets.svgs.profil),
      //       activeIcon: SvgPicture.asset($appAssets.svgs.profilActive)
      //     ),
      //   ],
      //
      // ),
    );
  }
}
