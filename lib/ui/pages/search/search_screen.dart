import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tp_arene/core/domain/locales/Slide.dart';
import 'package:tp_arene/export.dart';
import '../../../core/provider/bottomNavigation/navigator_provider.dart';
import '../../../core/router/route_utils.dart';
import '../../common/home/hot_cards.dart';
class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final TextEditingController _textEditingController = TextEditingController() ;
  bool _isFocused = false;

  @override
  Widget build(BuildContext context) {
    final homeDashBoardProvider = Provider.of<DashboardProvider>(context);

    return Padding(
        padding: const EdgeInsets.only(left: 24,right: 24,top: 38,bottom: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
            Container(
              height: 64,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24),
                color: AppColors.inputBorder,
              ),
              child: Center(
                child: Focus(
                  onFocusChange: (hasFocus) {
                    setState(() {
                      _isFocused = hasFocus;
                    });
                  },
                  child: InkWell(
                    onTap: (){
                      homeDashBoardProvider.setPath(AppPage.searchPage);
                       //context.pushReplacement(AppPage.searchPage.toPath);
                      //context.push("/search/searchPage");
                    },
                    child: IgnorePointer(
                      child: TextFormField(
                        controller: _textEditingController,
                        keyboardType: TextInputType.text,
                        cursorColor:  AppColors.primaryColor,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Search recipes, articles, people...",
                          hintStyle: AppTypography().text.copyWith(fontSize: 16,color: AppColors.hintText),
                          prefixIcon:  Icon(Icons.search,color: _isFocused?AppColors.primaryColor:AppColors.hintText),
                          iconColor: AppColors.primaryColor,
                        ),
                      ),
                    ),
                  )

                ),
              )
            ),
          const SpaceH(24),
          Text(
            "Hot Now",
            style: AppTypography().body,
            textAlign: TextAlign.left,
          ),
          const SpaceH(16),
          SizedBox(
            height: 231,
            child: ListView.builder(
                itemCount: myHot.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context,index){
                  return HotCards(img: myHot[index].img, title: myHot[index].title, subtitle: myHot[index].subtitle,);
                }
            ),
          ),
          const SpaceH(24),
          Text(
            "Trending",
            style: AppTypography().body,
            textAlign: TextAlign.left,
          ),
         Expanded(child:  SizedBox(
           height: 192,
           child: ListView.builder(
               itemCount: myTrending.length,
               itemBuilder: (context,index){
                 return Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                   Row(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     Text(
                       myTrending[index],
                       style: AppTypography().hintext.copyWith(color: AppColors.text1),
                       textAlign: TextAlign.left,
                     ),
                     const SpaceW(7),
                     const Icon(Icons.insights_outlined,color: AppColors.text1,),
                     //Image.asset($appAssets.imgs.data)
                   ],
                 ),
                     const SpaceH(16),
                     const Divider(),
                   ],
                 );               }
           ),
         ))





        ],
      ),
    );
  }
}
