import 'package:flutter/material.dart';
import 'package:tp_arene/core/router/route_utils.dart';
import 'package:tp_arene/export.dart';

import '../../../core/domain/locales/Slide.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final TextEditingController _textEditingController = TextEditingController();
  bool _isFocused = false;
  String trend ="";
  @override
  Widget build(BuildContext context) {
    final homeDashBoardProvider = Provider.of<DashboardProvider>(context);
    List<String> fieldOfStudyOnSearch = [];
    return SafeArea(
        child: Scaffold(
            backgroundColor: AppColors.background,
            appBar: AppBar(
              centerTitle: true,
              elevation: 0,
              backgroundColor: AppColors.background,
              leading: IconButton(
                  onPressed: () {
                    homeDashBoardProvider.setPath(AppPage.search);
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: AppColors.text,
                  )),
              title: Text(
                "Search",
                style: AppTypography().title.copyWith(color: AppColors.black),
                textAlign: TextAlign.left,
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.only(
                  top: 8, right: 24, left: 24, bottom: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 64,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24),
                        color: AppColors.inputBorder,
                      ),
                      child: Center(
                        child: Focus(
                            onFocusChange: (hasFocus) {
                              setState(() {
                                _isFocused = hasFocus;
                              });
                            },
                            child: InkWell(
                              onTap: () {
                                homeDashBoardProvider
                                    .setPath(AppPage.searchPage);
                              },
                              child: TextFormField(
                                autofocus: true,
                                onChanged: (value) {
                                  if (_textEditingController.text.isEmpty) {
                                    _textEditingController.clear();
                                    fieldOfStudyOnSearch.clear();
                                  }

                                  setState(() {
                                    fieldOfStudyOnSearch = myTrending
                                        .where((element) => element
                                        .toLowerCase()
                                        .contains(value.toLowerCase()))
                                        .toList();
                                  });
                                },
                                controller: _textEditingController,
                                keyboardType: TextInputType.text,
                                cursorColor: AppColors.primaryColor,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText:
                                  "Search recipes, articles, people...",
                                  hintStyle: AppTypography().text.copyWith(
                                      fontSize: 16, color: AppColors.hintText),
                                  prefixIcon: Icon(Icons.search,
                                      color: _isFocused
                                          ? AppColors.primaryColor
                                          : AppColors.hintText),
                                  iconColor: AppColors.primaryColor,
                                  // suffixIcon:  Container(
                                  //   margin: EdgeInsets.only(right: 20,bottom: 20),
                                  //   height: 24,
                                  //   width: 24,
                                  //   decoration: BoxDecoration(
                                  //     borderRadius: BorderRadius.circular(8),
                                  //     color: AppColors.clear,
                                  //   ),
                                  //   child: Icon(Icons.clear_rounded,color: _isFocused?AppColors.primaryColor:AppColors.background),
                                  // ),
                                ),
                              ),
                            )),
                      )),
                  _textEditingController.text.isNotEmpty &&
                      fieldOfStudyOnSearch.isEmpty
                      ?
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SpaceH(54),

                        Center(child: SvgPicture.asset($appAssets.svgs.searchNotFound),),
                        const SpaceH(24),
                        Text("No Results Found",
                            style: AppTypography()
                                .body
                                .copyWith(color: AppColors.noFound)),
                        const SpaceH(8),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 36),
                          child: Text(
                            "Try searching for a different keywork or tweek your search a little",
                            style: AppTypography()
                                .hintext
                                .copyWith(color: AppColors.gray),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  )
                      : Container(
                    padding: const EdgeInsets.only(
                        left: 22, right: 22, top: 16),
                    alignment: Alignment.topLeft,
                    child: Text("Résultats de la recherche:",
                        style: AppTypography()
                            .body
                            .copyWith(color: AppColors.text)),
                  ),
                  const SpaceH(16),
                  Expanded(child:
                  SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: InkWell(
                        onTap: () {},
                        child: ListView.separated(
                          separatorBuilder: (context, index) {
                            // _textEditingController.text.isNotEmpty;
                                // ? trend = fieldOfStudyOnSearch[index]
                                // : trend = myTrending[index];
                            return const SizedBox(
                              height: 15,
                            );
                          },
                          itemCount: _textEditingController.text.isNotEmpty
                              ? fieldOfStudyOnSearch.length
                              : myTrending.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {

                              },
                              child: Padding(
                                padding: const EdgeInsets.only(right: 20, left: 20),
                                child: _textEditingController.text.isNotEmpty
                                    ? Text(fieldOfStudyOnSearch[index]):Text(myTrending[index]),
                              ),

                            );
                          },
                        ),
                      )),)
                  ,                ],
              ),
            )));
  }
}
