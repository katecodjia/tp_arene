import 'package:flutter/material.dart';
import 'package:tp_arene/core/domain/locales/tile.dart';
import 'package:tp_arene/export.dart';
import 'package:tp_arene/ui/common/button/app_button.dart';

import '../../../common/home/custom_card.dart';
import '../../../common/home/no_found.dart';
class FoodTabs extends StatefulWidget {
  const FoodTabs({Key? key}) : super(key: key);

  @override
  State<FoodTabs> createState() => _FoodTabsState();
}

class _FoodTabsState extends State<FoodTabs> {
  @override
  Widget build(BuildContext context) {
    return favorite.isNotEmpty?
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SpaceH(24),
         Expanded(
             child: SizedBox(
               height: MediaQuery.of(context).size.height/0.7,
               width: double.infinity,
               child:  Wrap(
                 runSpacing: 16,
                 spacing: 16,
                 children: [
                   Wrap(
                     alignment: WrapAlignment.start,
                     runSpacing: 16,
                     spacing: 16,
                     direction: Axis.horizontal,
                     children: favorite.map((e) =>  CustomCards(
                       right: 0,
                       height: 96,
                       width: 96,
                       radius: BorderRadius.circular(16),
                       background: AppColors.cardColor,
                       img: e,
                       onTap: () {  }, twoPart: false,)).toList(),
                   ),
                   Container(
                     height: 96,
                     width: 96,
                     padding: const EdgeInsets.all(15),
                     decoration: BoxDecoration(
                       borderRadius:BorderRadius.circular(16),
                       color: AppColors.cardColor,
                     ),
                     child: Center(
                         child: InkWell(
                           onTap: (){},
                           child: const Icon(Icons.add,color: AppColors.yellow,size: 34,),
                         )
                     ),
                   )
                 ],
               )

             )
         ),
      ],
    ):Found(img: $appAssets.svgs.foodNotFound, title: 'No Foods Found', subtitle: 'You don’t save any food. Go ahead, search and save your favorite food', textButton: 'Search Food',);

  }
}
