import 'package:flutter/material.dart';
import 'package:tp_arene/core/domain/locales/Slide.dart';
import 'package:tp_arene/export.dart';
import 'package:tp_arene/ui/common/home/no_found.dart';

import '../../../common/button/app_button.dart';
import '../../../common/favorites/recipes_cards.dart';
class RecipesTabs extends StatefulWidget {
  const RecipesTabs({Key? key}) : super(key: key);

  @override
  State<RecipesTabs> createState() => _RecipesTabsState();
}

class _RecipesTabsState extends State<RecipesTabs> {
  @override
  Widget build(BuildContext context) {
     return recipes.isNotEmpty?
    Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SpaceH(24),

        Expanded(
            child: SizedBox(
                height: MediaQuery.of(context).size.height/0.7,
                width: double.infinity,
                child:ListView.builder(
                  itemCount: recipes.length,
                    itemBuilder: (context,index){
                      return RecipesCards(
                        onTap: () {  },
                        data: recipes[index],
                      );
                    }
                ),

            )
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: AppButton(text: "Search Recipes", onTap: (){}),
        ),

      ],
    ):Found(img: $appAssets.svgs.foodNotFound, title: "No Recipes Found", subtitle: "You don’t save any recipes. Go ahead, search and save your favorite recipe.", textButton: "Search Recipes");

  }
}
