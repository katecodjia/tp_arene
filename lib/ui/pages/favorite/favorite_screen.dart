import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';
class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> with TickerProviderStateMixin{
   late TabController _controller ;
   int index = 0;
   @override
  void initState() {
    // TODO: implement initState
     _controller = TabController(length: 2, vsync: this);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
     appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.background,
        title: Text(
          "Favorites",
          style: AppTypography().title.copyWith(color: AppColors.black),
          textAlign: TextAlign.center,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 21,right: 21,bottom: 20),
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: AppColors.cardColor,
                borderRadius: BorderRadius.circular(16),
              ),
              child: DefaultTabController(
                  length: 2,
                  child: TabBar(
                    onTap: (value){
                      return setState(() {
                        index = value;
                      });
                    },
                    indicator:  BoxDecoration(
                        color: AppColors.secondary,
                        borderRadius:index==1?
                        const BorderRadius.only(topRight: Radius.circular(16),bottomRight: Radius.circular(16))
                        :const BorderRadius.only( topLeft: Radius.circular(16),bottomLeft: Radius.circular(16))
                    ),
                    labelColor: AppColors.background,
                    unselectedLabelColor: AppColors.text1,
                    controller: _controller,
                    tabs: const [
                      Tab(text: "Food",),
                      Tab(text: "Recipes",),
                    ],
                  )
              ),
            ),

            Expanded(child: SizedBox(
              height: MediaQuery.of(context).size.height/0.7,
              child:  TabBarView(
                  controller: _controller,
                  children: const [
                    FoodTabs(),
                    RecipesTabs(),
                  ]
              ),)
            )
          ],
        ),
      ),
    );
  }
}
