export 'splash_screen/splash_screen.dart';
export 'intro/intro.dart';
export 'navigation_bar/navigation.dart';
export 'favorite/favorite.dart';
export 'profil/profile.dart';
export 'scan/scan.dart';
export 'search/search.dart';
export 'home/home.dart';