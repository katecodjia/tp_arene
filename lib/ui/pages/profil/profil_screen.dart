import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';

import '../../common/profile/customtile.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 17),
      child:  SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Text(
                  "Profile",
                  style: AppTypography().title.copyWith(color: AppColors.black),
                ),
              ),
              const SpaceH(36),
              Stack(
                children: [
                  Container(
                    height: 160,
                    width: 160,
                    decoration: const BoxDecoration(
                      color: AppColors.background,
                      shape: BoxShape.circle,
                    ),
                    child: Image.asset(
                      $appAssets.imgs.profil,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    top: 125,
                    left: 127,
                    child: Container(
                      padding: const EdgeInsets.all(6),
                      height: 24,
                      width: 24,
                      decoration: const BoxDecoration(
                        color: AppColors.secondary,
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset($appAssets.svgs.profilStatus),
                    ),
                  ),
                ],
              ),
              const SpaceH(24),
              Text("Shambhavi Mishra", style: AppTypography().kHeading20),
              const SpaceH(4),
              Text("Food Blogger",
                  style: AppTypography().hintext.copyWith(color: AppColors.gray)),
              const SpaceH(24),
              CustomTile(
                img: $appAssets.svgs.icProfil,
                title: 'Edit Profile',
                onTap: () {},
              ),
              const SpaceH(12),
              CustomTile(
                img: $appAssets.svgs.rate,
                title: 'Renew Plans',
                onTap: () {},
              ),
              const SpaceH(12),
              CustomTile(
                img: $appAssets.svgs.setting,
                title: 'Settings',
                onTap: () {},
              ),
              const SpaceH(12),
              CustomTile(
                img: $appAssets.svgs.terms,
                title: 'Terms & Privacy Policy',
                onTap: () {},
              ),
              const SpaceH(12),
              CustomTile(
                img: $appAssets.svgs.logout,
                title: 'Log Out',
                onTap: () {},
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
