import 'package:flutter/material.dart';
import 'dart:async';
import 'package:camera/camera.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:tp_arene/export.dart';

import '../../../core/router/route_utils.dart';
import 'overlay_page.dart';

class ScanScreen extends StatefulWidget {
  const ScanScreen({super.key});

  @override
  _ScanScreenState createState() => _ScanScreenState();
}

class _ScanScreenState extends State<ScanScreen> {
  CameraController? _controller;
  List<CameraDescription>? _cameras;
  bool _isFlashOn = false;
  bool _screenOpened = false;
  MobileScannerController cameraController = MobileScannerController();

  @override
  void initState() {
    super.initState();
    _initializeCamera();
  }

  Future<void> _initializeCamera() async {
    _cameras = await availableCameras();
    _controller = CameraController(_cameras![0], ResolutionPreset.medium);
    await _controller?.initialize();
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  void _toggleFlash() {
    setState(() {
      _isFlashOn = !_isFlashOn;
      _controller?.setFlashMode(_isFlashOn ? FlashMode.torch : FlashMode.off);
    });
  }
  //
  // Future<void> scanQRCode() async {
  //   try {
  //     final ScanResult result = await BarcodeScanner.scan();
  //     if (result.type == ResultType.Barcode) {
  //       final String scannedValue = result.rawContent;
  //       // Process the scanned value
  //       print('Scanned QR code: $scannedValue');
  //     }
  //   } on PlatformException catch (e) {
  //     if (e.code == BarcodeScanner.cameraAccessDenied) {
  //       // Handle camera permission denied
  //       print('Camera permission denied');
  //     } else {
  //       // Handle other errors
  //       print('Error: ${e.message}');
  //     }
  //   } on FormatException {
  //     // Handle user pressing back button without scanning anything
  //     print('User pressed back button');
  //   } catch (e) {
  //     // Handle other exceptions
  //     print('Error: $e');
  //   }
  // }


  @override
  Widget build(BuildContext context) {
    if (_controller == null || !_controller!.value.isInitialized) {
      return const Center(child: CircularProgressIndicator());
    }
    final homeDashBoardProvider = Provider.of<DashboardProvider>(context);

    var scanArea = (MediaQuery.of(context).size.width < 400 ||
        MediaQuery.of(context).size.height < 400)
        ? 200.0
        : 300.0;
    return Scaffold(
        body: Stack(
      children: [
        Positioned.fill(
          child: Container(
              height: 200,
              width: MediaQuery.of(context).size.width,
              decoration: ShapeDecoration(
                shape: QrScannerOverlayShape(
                  borderColor: AppColors.inputBorder,
                  borderRadius: 0,
                  borderLength: 12,
                  borderWidth: 6,
                  cutOutSize: scanArea,
                ),
              ),
              child: MobileScanner(
                allowDuplicates: true,
                controller: cameraController,
                onDetect: _foundBarcode,
                fit: BoxFit.cover,
              )
          ),

          // AspectRatio(
          //   aspectRatio: _controller!.value.aspectRatio,
          //   child: CameraPreview(_controller!),
          // ),
        ),
        Positioned(
            bottom: 0,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              height: 168,
              width: MediaQuery.of(context).size.width,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(32)),
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 40,
                    width: 40,
                    child: Image.asset($appAssets.imgs.preview),
                  ),
                  Container(
                    height: 98,
                    decoration: const BoxDecoration(
                        color: AppColors.background,
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(32))),
                    child: Container(
                        height: 72,
                        width: 72,
                        padding: const EdgeInsets.all(16),
                        decoration: const BoxDecoration(
                          color: AppColors.dotColor,
                          shape: BoxShape.circle,
                        ),
                        child: InkWell(
                          onTap: () {},
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: const BoxDecoration(
                              color: AppColors.text1,
                              shape: BoxShape.circle,
                            ),
                          ),
                        )),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                      color: AppColors.cardColor,
                    ),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _initializeCamera();
                        });
                      },
                      child: const Icon(Icons.cached),
                    ),
                  )
                ],
              ),
            )),
        Positioned(
            top: 38,
            right: 32,
            left: 32.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    onPressed: () {
                      homeDashBoardProvider.setPath(AppPage.home);
                    },
                    icon: const Icon(
                      Icons.close,
                      color: AppColors.background,
                    )),
                IconButton(
                  onPressed: _toggleFlash,
                  icon: Icon(_isFlashOn ? Icons.flash_on : Icons.flash_off),
                  color: AppColors.background,
                )
              ],
            )),
      ],
    ));
  }
  void _foundBarcode(Barcode barcode, MobileScannerArguments? args) {
    /// open screen
    if (!_screenOpened) {
      final String code = barcode.rawValue ?? "---";
      debugPrint('Barcode found! $code');
      _screenOpened = true;
    }
  }
}

void main() {
  runApp(MaterialApp(
    home: ScanScreen(),
  ));
}
