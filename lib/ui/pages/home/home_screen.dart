import 'dart:async';

import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:tp_arene/core/domain/locales/tile.dart';
import 'package:tp_arene/export.dart';

import '../../common/button/app_button.dart';
import '../../common/home/bottomSheet.dart';
import '../../common/home/custom_card.dart';
import '../../common/home/home_slide.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  final List<dynamic> letters = [
    const SlideHome(),
    const SlideHome(),
    const SlideHome(),
  ];
   PageController _pageController = PageController();
  int _currentPage = 0;
  late Timer _timer;
  final int _pageCount = 3;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(initialPage: _currentPage);
    _startAutoScroll();
  }
  @override
  void dispose() {
    _pageController.dispose();
    _stopAutoScroll();
    super.dispose();
  }
  void _startAutoScroll() {
    _timer = Timer.periodic(const Duration(seconds: 3), (_) {
      if (_currentPage < _pageCount - 1) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }
      _pageController.animateToPage(
        _currentPage,
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  void _stopAutoScroll() {
    _timer.cancel();
  }
  @override
  Widget build(BuildContext context) {

    return  SingleChildScrollView(
      child: Container(
        color: AppColors.background,
        padding:const EdgeInsets.only(right:28,top: 46,bottom: 18,left: 28),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Hello Shambhavi,",
              style: AppTypography().kHeading24.copyWith(color: AppColors.primaryColor),
              textAlign: TextAlign.center,
            ),
            Text(
              "Find, track and eat heathy food.",
              style: AppTypography().text.copyWith(color: AppColors.greyText,fontSize: 18),
              textAlign: TextAlign.center,
            ),
            const SpaceH(16),
            SizedBox(
              height: 169,
              child: PageView.builder(
                controller: _pageController,
                itemCount: letters.length,
                physics: const ClampingScrollPhysics(),
                itemBuilder: (context,index){
                  return  letters[index];
                },
                onPageChanged: (index) {
                  setState(() {
                    _currentPage = index;
                  });
                },
              ),
            ),
            const SpaceH(10),
            SmoothPageIndicator(
              count: letters.length,
              controller: _pageController,
              effect:  const ExpandingDotsEffect(
                  dotHeight: 8,
                  dotWidth: 8,
                  spacing: 4.0,
                  dotColor: AppColors.dotColor,
                  activeDotColor: AppColors.text1,
                  strokeWidth: 1,
                  paintStyle: PaintingStyle.fill),
              onDotClicked: (index) => _pageController.animateToPage(index,
                  duration: const Duration(
                    milliseconds: 500,
                  ),
                  curve: Curves.easeIn),
            ),
            const SpaceH(16),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 19,horizontal: 32),
              height: 88,
              width: 351.88,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(24),
                  color: AppColors.violet
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(child: Text(
                    "Track Your Weekly Progress",
                    style: AppTypography().kHeading26.copyWith(color: AppColors.background,fontSize: 18),
                    textAlign: TextAlign.left,
                  ),),
                  const SpaceW(20),
                  AppButton(
                      text: '',
                      onTap: () {  },
                      backgroundColor: AppColors.background,
                      height: 32,
                      width: 98,
                      raduis: BorderRadius.circular(8),
                      widget: Padding(
                        padding: const EdgeInsets.only(left: 16,top: 6,bottom: 6),
                        child: Row(
                          children: [
                            Text(
                              "View Now",
                              style: AppTypography().title.copyWith(color: AppColors.lightViolet,fontWeight: FontWeight.w600),
                            ),
                            const SpaceW(7),
                            const Icon(Icons.arrow_right,color: AppColors.lightViolet,size: 20,)

                          ],
                        ),
                      )
                  )


                ],
              ),
            ),
            const SpaceH(24),
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Choose Your Favorites",
                style: AppTypography().body,
                textAlign: TextAlign.left,
              ),
            ),
            const SpaceH(24),
            SizedBox(
              height: 144,
              child: ListView.builder(
                itemCount: myFavorite.length,
                scrollDirection: Axis.horizontal,
                  itemBuilder: (context,index){
                  return CustomCards(
                    img: myFavorite[index].img,
                    label: myFavorite[index].text,
                    onTap: () {
                            showModalBottomSheet(
                              isScrollControlled: true,
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.vertical(top: Radius.circular(32))
                                ),
                                backgroundColor: AppColors.background,
                                context: context,
                                builder: (context){
                                  return CustomBottomSheet(data: myFavorite[index],);
                                }
                            );
                    },);
                  }
              ),
            )


          ],
        ),
      ),
    );
  }
}
