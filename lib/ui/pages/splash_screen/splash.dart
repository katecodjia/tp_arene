import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';
import '../../../core/router/route_utils.dart';
import '../../../core/service/app/app_services.dart';
class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    onStartUp();
  }
  void onStartUp() async {
    await AppService.instance.onAppStart();
    _timer = Timer(const Duration(seconds: 2), () {
      if (AppService.instance.initialized) {
        context.pushReplacement(AppPage.introScreen.toPath);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(child: Center(
            child: Image.asset($appAssets.imgs.logo,),
          ),),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.only(bottom: 20),
              child: Text(
                'ZUAMICA',
                style: AppTypography().kHeading24.copyWith(fontWeight: FontWeight.w800,color: AppColors.bottom),
              ),
            )
          ),


        ],
      ),
    );
  }


}
