import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:tp_arene/core/domain/locales/Slide.dart';
import 'package:tp_arene/export.dart';
import '../../../core/router/route_utils.dart';
import '../../common/button/app_button.dart';
import '../../common/introScreen/intro_cards.dart';
class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Container(
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.only(left: 40,right: 40,bottom: 20,top: 44),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Image.asset($appAssets.imgs.logo,color: AppColors.primaryColor,width: 58,),),
            //const SpaceH(26),
            Expanded(child: SizedBox(
              height: 400,
              child:  PageView.builder(
                controller: _pageController,
                itemCount: myList.length,
                physics: const ClampingScrollPhysics(),
                itemBuilder: (context,index){
                  return  IntroCards(slide: myList[index],);
                },
              ),
            ),),
            const SpaceH(13),
      SmoothPageIndicator(
        count: myList.length,
        controller: _pageController,
        effect:  const ExpandingDotsEffect(
            dotHeight: 8,
            dotWidth: 8,
            spacing: 4.0,
            dotColor: AppColors.dotColor,
            activeDotColor: AppColors.text1,
            strokeWidth: 1,
            paintStyle: PaintingStyle.fill),
        onDotClicked: (index) => _pageController.animateToPage(index,
            duration: const Duration(
              milliseconds: 500,
            ),
            curve: Curves.easeIn),
      ),
            const SpaceH(32),
            AppButton(text: 'Get Started', onTap: () {
              context.go(AppPage.dashboard.toPath);

              // Navigator.pushReplacement(
              //   context,
              //   MaterialPageRoute(builder: (context) => const BottomNavigation()),
              // );
            },),
            const SpaceH(16),
            RichText(
              text: TextSpan(
                text: 'Already Have An Account?',
                style: AppTypography().kHeading26,
                children:  <TextSpan>[
                  TextSpan(text: ' Log In', style: AppTypography().text.copyWith(color: AppColors.primaryColor)),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
