import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tp_arene/export.dart';
class CameraApp extends StatefulWidget {
  /// Default Constructor
   CameraApp({Key? key, this.imageFile,required this.upload}) : super(key: key);
  XFile? imageFile;
   Function(XFile? image) upload;

  @override
  State<CameraApp> createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp> {
  final ImagePicker _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 98,
            decoration: const BoxDecoration(
              color: AppColors.background,
                borderRadius: BorderRadius.vertical(top: Radius.circular(32))
            ),
            child: Center(
              child: Container(
                height: 72,
                width: 72,
                padding: const EdgeInsets.all(16),
                decoration: const BoxDecoration(
                  color: AppColors.dotColor,
                  shape: BoxShape.circle,
                ),
                child: InkWell(
                  onTap: ()async{
                    takePicture(ImageSource.camera,context,);
                  },
                  child: Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                      color: AppColors.text1,
                      shape: BoxShape.circle,
                    ),
                  ),
                )
              ),
            ),
          ),
        ),

      );


  }
  void takePicture(ImageSource source, BuildContext context) async{

    final XFile? image = await _picker.pickImage(source: source);
    if(image != null){
      if(image.path != null || image.path.isEmpty){
        widget.imageFile = image;
        widget.upload(image);
      }else{
        Navigator.pop(context);
      }
    }
  }

}
