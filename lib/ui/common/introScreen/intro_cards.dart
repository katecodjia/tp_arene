import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';

import '../../../core/domain/locales/Slide.dart';
import '../../../ressources/theme/color/app_color.dart';
import '../../../ressources/theme/typography/app_typography.dart';
import '../../../utils/constants/space.dart';
class IntroCards extends StatefulWidget {
  const IntroCards({Key? key, required this.slide}) : super(key: key);
final Slide slide;
  @override
  State<IntroCards> createState() => _IntroCardsState();
}

class _IntroCardsState extends State<IntroCards> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SvgPicture.asset(widget.slide.img,height: 282,),
        const SpaceH(24),
        Expanded(
          child: AutoSizeText(widget.slide.title,style: AppTypography().kHeading24,textAlign: TextAlign.center,maxLines: 1, overflow: TextOverflow.ellipsis,),),
        const SpaceH(8),
        Expanded(
          child: Text(widget.slide.subtitle,style: AppTypography().text.copyWith(color: AppColors.text.withOpacity(0.45)),textAlign: TextAlign.center,maxLines: 2, overflow: TextOverflow.ellipsis,)
          ,),

      ],
    );
  }
}
