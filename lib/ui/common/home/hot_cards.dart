import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';
class HotCards extends StatelessWidget {
  const HotCards({Key? key, required this.img, required this.title, required this.subtitle}) : super(key: key);

  final String img;
  final String title;
  final String subtitle;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 16),
      height: 231,
      width:200,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 159,
            width: 200,
            child: Image.asset(img,fit: BoxFit.fill,),
          ),
          const SpaceH(7),
          Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: AppTypography().body.copyWith(fontSize: 14),
                  textAlign: TextAlign.left,
                ),
                Text(
                  subtitle,
                  style: AppTypography().subtitle,
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          )

        ],
      )
      ,
    );
  }
}
