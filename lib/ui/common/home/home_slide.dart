import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';

import '../button/app_button.dart';
class SlideHome extends StatelessWidget {
  const SlideHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return   Stack(
      alignment: Alignment.topLeft,
      children: [
        Container(
          padding: const EdgeInsets.all(31),
          height: 169,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(32),
            gradient: const LinearGradient(
              begin: Alignment(-0.6, -0.6),
              end: Alignment(0.5, 0.5),
              stops: [0.0348, 1.0424],
              colors: [
                Color.fromRGBO(255, 242, 240, 0.5),
                Color(0xFFFFF8EB),
              ],
              transform: GradientRotation(106.1 * 3.141592 / 180),
            ),
          ),

        ),

        Positioned(
          top: 31,
          left: 32,
          child: Text(
            "A R T I C L E",
            style: AppTypography().kHeading26.copyWith(color: AppColors.text1,fontSize: 10),
            textAlign: TextAlign.center,
          ),
        ),
        Positioned(
            top: 49,
            left: 30,
            child: SizedBox(
              //height: 48,
              width: 125,
              child: AutoSizeText(
                "The pros and cons of fast food.",
                style: AppTypography().kHeading26.copyWith(color: AppColors.textBlack),
                textAlign: TextAlign.left,
                maxLines: 2,
              ),
            )
        ),
        Positioned(
            top: 105,
            left: 32,
            child: AppButton(
                text: '',
                onTap: () {  },
                backgroundColor: AppColors.text1,
                height: 32,
                width: 104,
                raduis: BorderRadius.circular(8),
                widget: Padding(
                  padding: const EdgeInsets.only(left: 19,top: 6,bottom: 6),
                  child: Row(
                    children: [
                      Text(
                        "Read Now",
                        style: AppTypography().title.copyWith(color: AppColors.background,fontWeight: FontWeight.w600),
                      ),
                      const SpaceW(7),
                      const Icon(Icons.arrow_right,color: AppColors.background,size: 20,)

                    ],
                  ),
                )
            )
        ),
        Positioned(
            top: 24,
            right: 32,
            child: SvgPicture.asset($appAssets.svgs.cheese)
        ),
      ],
    );

  }
}
