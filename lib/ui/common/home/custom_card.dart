import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';
class CustomCards extends StatelessWidget {
   CustomCards({Key? key,this.twoPart = true, this.height, this.width, this.radius, required this.img,  this.label, this.background, this.labelColor, required this.onTap, this.right}) : super(key: key);
  final double? height;
  final double? width;
  final double? right;
  final BorderRadius? radius;
  final String img;
  final  String? label;
  final Color? background;
  final Color? labelColor;
   final VoidCallback onTap;
    bool twoPart = true;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin:  EdgeInsets.only(right: right?? 16),
        height: height??144,
        width: width??132,
        decoration: BoxDecoration(
          borderRadius: radius??BorderRadius.circular(32),
          color: background??AppColors.pink,
        ),
        child: Center(
          child: twoPart? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(img),
               const SpaceH(12),
                Text(
                label??'',
                style: AppTypography().text.copyWith(color: labelColor?? AppColors.lightGray),
                textAlign: TextAlign.center,
              )

            ],
          ):
          SvgPicture.asset(img,height: 40,width: 40,),
        ),
      ),
    );
  }
}
