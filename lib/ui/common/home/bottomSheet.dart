import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';

import '../../../core/domain/locales/tile.dart';
import '../button/app_button.dart';
import 'custom_card.dart';
class CustomBottomSheet extends StatefulWidget {
  const CustomBottomSheet({Key? key, required this.data}) : super(key: key);
  final Tile data;
  @override
  State<CustomBottomSheet> createState() => _CustomBottomSheetState();
}

class _CustomBottomSheetState extends State<CustomBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height:MediaQuery.of(context).size.height * 0.90,
      child: ListView(
        children: [
          const SpaceH(22),
          Align(
            alignment: Alignment.topLeft,
            child: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: const Icon(Icons.close,color: AppColors.text,),
            ),
          ),


          const SpaceH(56),
          Center(
            child: SizedBox(
              height: 140,
              width: 140,
              child:  SvgPicture.asset(widget.data.img),
            ),
          ),
          const SpaceH(14),
          Container(
            padding: const EdgeInsets.all(28),
            height: 110,
            color:AppColors.cardColor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      "Protein",
                      style: AppTypography().hintext.copyWith(color: AppColors.text1),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      widget.data.protein??"",
                      style: AppTypography().body.copyWith(fontSize: 24,color: AppColors.text1),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      "Calories",
                      style: AppTypography().hintext.copyWith(color: AppColors.text1),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      widget.data.calorie??"",
                      style: AppTypography().body.copyWith(fontSize: 24,color: AppColors.text1),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      "Fat",
                      style: AppTypography().hintext.copyWith(color: AppColors.text1),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      widget.data.fat??"",
                      style: AppTypography().body.copyWith(fontSize: 24,color: AppColors.text1),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      "Carbs",
                      style: AppTypography().hintext.copyWith(color: AppColors.text1),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      widget.data.carbs??"",
                      style: AppTypography().body.copyWith(fontSize: 24,color: AppColors.text1),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ],

            ),
          ),
          const SpaceH(24),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:32 ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Details",
                  style: AppTypography().body,
                  textAlign: TextAlign.left,
                ),
                const SpaceH(8),
                RichText(
                  text: TextSpan(
                    text:   widget.data.details??"",
                    style:  AppTypography().hintext.copyWith(color: AppColors.text2),
                    children:  <TextSpan>[
                      TextSpan(
                        text: "  Read More...",
                        style: AppTypography().kHeading26.copyWith(fontSize: 16,color: AppColors.primaryColor),),
                    ],
                  ),
                ),
                const SpaceH(16),
                Text(
                  "Ingredients",
                  style: AppTypography().body,
                  textAlign: TextAlign.left,
                ),
                const SpaceH(16),

                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 64,
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: myIngredient.length + 1,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index){
                        return index< myIngredient.length?
                        CustomCards(
                          height: 64,
                          width: 64,
                          radius: BorderRadius.circular(16),
                          background: AppColors.cardColor,
                          img: myFavorite[index].ingr?[index].image??"",
                          onTap: () {  }, twoPart: false,):
                        Container(
                          height: 64,
                          width: 64,
                          padding: const EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            borderRadius:BorderRadius.circular(16),
                            color: AppColors.cardColor,
                          ),
                          child: Center(
                              child: InkWell(
                                onTap: (){},
                                child: Text("View All",maxLines: 2,style: AppTypography().title.copyWith(fontWeight: FontWeight.w400,color: AppColors.text1),textAlign: TextAlign.center,),
                              )

                          ),
                        );
                      }
                  ),

                ),



                    // SizedBox(
                    //   height: 64,
                    //   width: MediaQuery.of(context).size.width,
                    //   child:ListView(
                    //     scrollDirection: Axis.horizontal,
                    //     children: [
                    //       SizedBox(
                    //         width:190,
                    //         child:
                    //         ListView.builder(
                    //             itemCount: myIngredient.length,
                    //             scrollDirection: Axis.horizontal,
                    //             itemBuilder: (context,index){
                    //               return
                    //                 CustomCards(
                    //                   height: 64,
                    //                   width: 64,
                    //                   radius: BorderRadius.circular(16),
                    //                   background: AppColors.cardColor,
                    //                   img: myFavorite[index].ingr?[index].image??"",
                    //                   onTap: () {  }, twoPart: false,);
                    //             }
                    //         ),
                    //       ),
                    //       Container(
                    //         height: 64,
                    //         width: 64,
                    //         padding: const EdgeInsets.all(15),
                    //         decoration: BoxDecoration(
                    //           borderRadius:BorderRadius.circular(16),
                    //           color: AppColors.cardColor,
                    //         ),
                    //         child: Center(
                    //             child: InkWell(
                    //               onTap: (){},
                    //               child: AutoSizeText("View All",maxLines: 2,style: AppTypography().title.copyWith(fontWeight: FontWeight.w400,color: AppColors.text1),textAlign: TextAlign.center,),
                    //             )
                    //
                    //         ),
                    //       )
                    //     ],
                    //   ),
                    // ),

                const SpaceH(19),
                AppButton(
                  text: 'Add To Favorites',
                  onTap: () {
                    setState(() {
                      favorite.add(widget.data.img);
                    });
                    Navigator.pop(context);
                  },
                ),
                const SpaceH(19),



              ],
            ),
          ),
        ],
      ),
    );
  }
}
