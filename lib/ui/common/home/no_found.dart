import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';

import '../button/app_button.dart';
class Found extends StatelessWidget {
  const Found({Key? key, required this.img, required this.title, required this.subtitle,  this.textButton,  this.isButton=true}) : super(key: key);
 final String  img;
  final String  title;
  final String  subtitle;
  final String?  textButton;
 final bool isButton;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(child:
                SvgPicture.asset(img),),
                const SpaceH(24),
                Text(title,
                    style: AppTypography()
                        .body
                        .copyWith(color: AppColors.noFound)),
                const SpaceH(8),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 36),
                  child: Text(
                    subtitle,
                    style: AppTypography()
                        .hintext
                        .copyWith(color: AppColors.gray),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            )
        ),

        isButton?
        Align(
          alignment: Alignment.bottomCenter,
          child: AppButton(text: textButton??"", onTap: (){}),
        ):SizedBox.shrink(),


      ],
    );
  }
}
