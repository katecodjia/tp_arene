import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';
class CustomTile extends StatefulWidget {
   CustomTile({Key? key, required this.img, required this.title,this.color,required this.onTap}) : super(key: key);
final String img;
final String title;
Color? color;
 final VoidCallback? onTap;
  @override
  State<CustomTile> createState() => _CustomTileState();
}

class _CustomTileState extends State<CustomTile> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      leading:  Container(
        padding: const EdgeInsets.all(12),
        height: 48,
        width: 48,
        decoration:  BoxDecoration(
          color: widget.color?? AppColors.cardColor,
          borderRadius: BorderRadius.circular(8),
        ),
        child: SvgPicture.asset(widget.img),
      ),
      title: Text(widget.title,style: AppTypography().hintext.copyWith(color: AppColors.text2,fontSize: 17)),
      trailing: IconButton(
          onPressed: (){},
          icon:  Icon(Icons.arrow_forward_ios,color: AppColors.greyColor.withOpacity(0.6),)
      ),
    );
    //   Row(
    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //   crossAxisAlignment: CrossAxisAlignment.start,
    //   children: [
    //     Row(
    //       children: [
    //         Container(
    //           padding: const EdgeInsets.all(12),
    //           height: 48,
    //           width: 48,
    //           decoration:  BoxDecoration(
    //             color: widget.color?? AppColors.cardColor,
    //             borderRadius: BorderRadius.circular(8),
    //           ),
    //           child: SvgPicture.asset(widget.img),
    //         ),
    //         const SpaceW(16),
    //         Text(widget.title,style: AppTypography().hintext.copyWith(color: AppColors.text2,fontSize: 17)),
    //
    //       ],
    //     ),
    //     IconButton(
    //         onPressed: (){},
    //         icon:  Icon(Icons.arrow_forward_ios,color: AppColors.greyColor.withOpacity(0.6),)
    //     ),
    //
    //
    //   ],
    // );

  }
}
