
import 'package:flutter/material.dart';
import 'package:tp_arene/export.dart';

import '../../../core/domain/locales/Slide.dart';
class RecipesCards extends StatelessWidget {
   const RecipesCards({Key? key, this.height, required this.onTap,  this.margin, required this.data}) : super(key: key);
   final VoidCallback onTap;
    final double? height;
    final double? margin;
    final Slide data;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
          height: height??120,
          padding: const EdgeInsets.only(left: 24,top: 16,right:16 ),
          margin: EdgeInsets.only(bottom: margin ?? 16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: AppColors.lightGreen,
          ),
          child: Column(
            children: [
              const Align(
                alignment: Alignment.topRight,
                child: Icon(Icons.favorite,color: AppColors.primaryColor,size: 16,),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SvgPicture.asset(data.img),
                  const SpaceW(20),
                  Expanded(child:
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(data.kcal??"",
                          style: AppTypography()
                              .kHeading22
                              .copyWith(color: AppColors.primaryColor)),
                      const SpaceH(4),
                      Text(data.title,
                          style: AppTypography()
                              .hintext
                              .copyWith(color: AppColors.text)),
                      const SpaceH(4),

                      Text(data.subtitle,
                          style: AppTypography()
                              .subtitle
                              .copyWith(color: AppColors.text2)),
                    ],
                  ))
                ],
              ),


            ],
          )
      ),
    );
  }
}
