import 'package:flutter/material.dart';
import 'package:tp_arene/ressources/theme/app_theme.dart';

import 'app_inkwell.dart';

class AppButton extends StatelessWidget {
  const AppButton(
      {Key? key,
      this.backgroundColor,
      required this.text,
      required this.onTap,
      this.textColor = Colors.white,
      this.height,
      this.textStyle,
      this.width,
      this.raduis,
      this.borderColor,
      this.widget,
      this.borderWith})
      : super(key: key);
  final String text;
  final VoidCallback? onTap;
  final Color? backgroundColor;
  final Color? textColor;
  final Color? borderColor;
  final double? height;
  final double? width;
  final double? borderWith;
  final BorderRadiusGeometry? raduis;
  final TextStyle? textStyle;
  final Widget? widget;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: width ?? double.infinity,
        height: height ?? 72,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: backgroundColor ?? AppColors.primaryColor,
          borderRadius: raduis ?? BorderRadius.circular(24),
          border: Border.all(
            color: borderColor ?? Colors.transparent,
            style: BorderStyle.solid,
            width: borderWith ?? 0,
          ),
        ),
        child: widget?? Text(
          text,
          style:
              textStyle ?? AppTypography().kHeading24.copyWith(color: AppColors.background,fontStyle: FontStyle.normal),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
